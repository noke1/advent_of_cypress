## Status of the challenges solved:
- Advent Of Cypress, Day 1: Challenge was to solve the spec08.js
- Advent Of Cypress, Day 2: Challenge was to solve the spec14.js
- Advent Of Cypress, Day 3: Challenge was to solve the lesson a1 and a3 from the repository with getByLabel plugin. I wont clone and push that repository to remote.
- Advent Of Cypress, Day 4: Challenge was to solve the spec04.js
