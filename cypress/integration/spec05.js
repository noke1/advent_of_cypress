/// <reference types="cypress" />

it('shows a different fruit after reloading the page', () => {
  // visit the site using https://on.cypress.io/visit
  // grab the fruit name from the page
  // (make sure it is not "loading...")
  cy.visit('/')
  cy.get('#fruit')
  .invoke('text')
  .should('not.equal', 'loading...')
  .as('fruitName')
  
  // reload the page using https://on.cypress.io/reload
  // grab the fruit name from the page again
  // confirm the fruit name is different
  cy.reload()
  // tip: use nested https://on.cypress.io/then callbacks
  cy.get('#fruit')
  .invoke('text')
  .should('not.equal', 'loading...')
  .as('fruitNameRefreshed')

  cy.then(function() {
    expect(this.fruitName).to.not.equal(this.fruitNameRefreshed)
  })
  // .then(fruitName => {
  //   cy.get('@fruitName').then(name => {
  //     expect(name).to.not.equal(fruitName)
  //   })
  // })


})


it('shows a different fruit after reloading the page - second way to assert', () => {
  // visit the site using https://on.cypress.io/visit
  // grab the fruit name from the page
  // (make sure it is not "loading...")
  cy.visit('/')
  cy.get('#fruit')
  .invoke('text')
  .should('not.equal', 'loading...')
  .as('fruitName')
  
  // reload the page using https://on.cypress.io/reload
  // grab the fruit name from the page again
  // confirm the fruit name is different
  cy.reload()
  // tip: use nested https://on.cypress.io/then callbacks
  cy.get('#fruit')
  .invoke('text')
  .should('not.equal', 'loading...')
  .then(fruitName => {
    cy.get('@fruitName').then(name => {
      expect(name).to.not.equal(fruitName)
    })
  })


})